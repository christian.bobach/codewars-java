package eu.bobach.codewars;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

@SuppressWarnings("PMD.TooManyMethods")
public class RomanNumeralsEncoderTest {
  @Test
  public void shouldConvertToRoman_1() {
    assertEquals("I", RomanNumeralsEncoder.solution(1));
  }

  @Test
  public void shouldConvertToRoman_5() {
    assertEquals("V", RomanNumeralsEncoder.solution(5));
  }

  @Test
  public void shouldConvertToRoman_10() {
    assertEquals("X", RomanNumeralsEncoder.solution(10));
  }

  @Test
  public void shouldConvertToRoman_50() {
    assertEquals("L", RomanNumeralsEncoder.solution(50));
  }

  @Test
  public void shouldConvertToRoman_100() {
    assertEquals("C", RomanNumeralsEncoder.solution(100));
  }

  @Test
  public void shouldConvertToRoman_500() {
    assertEquals("D", RomanNumeralsEncoder.solution(500));
  }

  @Test
  public void shouldConvertToRoman_1000() {
    assertEquals("M", RomanNumeralsEncoder.solution(1000));
  }

  @Test
  public void shouldConvertToRoman_4() {
    assertEquals("IV", RomanNumeralsEncoder.solution(4));
  }

  @Test
  public void shouldConvertToRoman_6() {
    assertEquals("VI", RomanNumeralsEncoder.solution(6));
  }

  @Test
  public void shouldConvertToRoman_1990() {
    assertEquals("MCMXC", RomanNumeralsEncoder.solution(1990));
  }

  @Test
  public void shouldConvertToRoman_2008() {
    assertEquals("MMVIII", RomanNumeralsEncoder.solution(2008));
  }

  @Test
  public void shouldConvertToRoman_1666() {
    assertEquals("MDCLXVI", RomanNumeralsEncoder.solution(1666));
  }
}
