package eu.bobach.codewars;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConvertStringToCamelCaseTest {
  @Test
  public void testSomeUnderscoreLowerStart() {
    String input = "the_Stealth_Warrior";
    Assertions.assertEquals("theStealthWarrior", ConvertStringToCamelCase.toCamelCase(input));
  }

  @Test
  public void testSomeUnderscoreUpperStart() {
    String input = "The_Stealth_Warrior";
    assertEquals("TheStealthWarrior", ConvertStringToCamelCase.toCamelCase(input));
  }

  @Test
  public void testSomeDashLowerStart() {
    String input = "the-Stealth-Warrior";
    assertEquals("theStealthWarrior", ConvertStringToCamelCase.toCamelCase(input));
  }

  @Test
  public void testSomeDashUpperStart() {
    String input = "The-Stealth-Warrior";
    assertEquals("TheStealthWarrior", ConvertStringToCamelCase.toCamelCase(input));
  }
}
