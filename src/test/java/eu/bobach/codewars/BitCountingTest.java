package eu.bobach.codewars;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public final class BitCountingTest {
  @Test
  public void test_0() {
    assertEquals(0, BitCounting.countBits(0));
  }

  @Test
  public void test_1() {
    assertEquals(1, BitCounting.countBits(1));
  }

  @Test
  public void test_2() {
    assertEquals(1, BitCounting.countBits(2));
  }

  @Test
  public void test_3() {
    assertEquals(2, BitCounting.countBits(3));
  }

  @Test
  public void test_1234() {
    assertEquals(5, BitCounting.countBits(1234));
  }

  @Test
  public void test_4() {
    assertEquals(1, BitCounting.countBits(4));
  }

  @Test
  public void test_7() {
    assertEquals(3, BitCounting.countBits(7));
  }

  @Test
  public void test_9() {
    assertEquals(2, BitCounting.countBits(9));
  }

  @Test
  public void test_10() {
    assertEquals(2, BitCounting.countBits(10));
  }
}
