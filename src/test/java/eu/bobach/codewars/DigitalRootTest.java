package eu.bobach.codewars;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public final class DigitalRootTest {
  @Test
  public void test_0() {
    assertEquals(0, DigitalRoot.digitalRoot(0));
  }

  @Test
  public void test_1() {
    assertEquals(1, DigitalRoot.digitalRoot(1));
  }

  @Test
  public void test_9() {
    assertEquals(9, DigitalRoot.digitalRoot(9));
  }

  @Test
  public void test_10() {
    assertEquals(1, DigitalRoot.digitalRoot(10));
  }

  @Test
  public void test_16() {
    assertEquals(7, DigitalRoot.digitalRoot(16));
  }

  @Test
  public void test_456() {
    assertEquals(6, DigitalRoot.digitalRoot(456));
  }

  @Test
  public void test_942() {
    assertEquals(6, DigitalRoot.digitalRoot(942));
  }

  @Test
  public void test_132189() {
    assertEquals(6, DigitalRoot.digitalRoot(132189));
  }

  @Test
  public void test_493193() {
    assertEquals(2, DigitalRoot.digitalRoot(493193));
  }
}
