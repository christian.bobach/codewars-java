package eu.bobach.codewars;

public final class DigitalRoot {
  private DigitalRoot() {
  }

  //  https://mathworld.wolfram.com/DigitalRoot.html
  public static int digitalRoot(int n) {
    int mod = n % 9;
    return (n == 0 || mod != 0) ? mod : 9;
  }
}
