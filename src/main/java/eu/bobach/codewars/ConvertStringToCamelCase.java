package eu.bobach.codewars;

import java.util.Locale;
import java.util.regex.Pattern;

public final class ConvertStringToCamelCase {
  private ConvertStringToCamelCase() {
  }

  public static String toCamelCase(String s) {
    return Pattern.compile("[-_](.)")
        .matcher(s)
        .replaceAll(r -> r.group(1)
            .toUpperCase(Locale.US));
  }
}
