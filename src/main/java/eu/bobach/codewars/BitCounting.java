package eu.bobach.codewars;

public final class BitCounting {
  private BitCounting() {
  }

  public static int countBits(final int n) {
    return Integer.bitCount(n);
  }
}
