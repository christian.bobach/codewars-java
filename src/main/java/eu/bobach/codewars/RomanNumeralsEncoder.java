package eu.bobach.codewars;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public final class RomanNumeralsEncoder {

  private static final Map<Integer, String> MAP = ImmutableMap.<Integer, String>builder()
      .put(1000, "M")
      .put(900, "CM")
      .put(500, "D")
      .put(400, "CD")
      .put(100, "C")
      .put(90, "XC")
      .put(50, "L")
      .put(40, "XL")
      .put(10, "X")
      .put(9, "IX")
      .put(5, "V")
      .put(4, "IV")
      .put(1, "I")
      .build();

  private RomanNumeralsEncoder() {
  }

  @SuppressWarnings("PMD")
  public static String solution(final int n) {
    int m = n;
    StringBuilder sb = new StringBuilder();
    for (Map.Entry<Integer, String> entry :
        MAP.entrySet()) {
      while (m >= entry.getKey()) {
        sb.append(entry.getValue());
        m -= entry.getKey();
      }
    }
    return sb.toString();
  }
}
