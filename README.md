# codewars-java

[![pipeline status](https://gitlab.com/christian.bobach/codewars-java/badges/master/pipeline.svg)](https://gitlab.com/christian.bobach/codewars-java/-/commits/master)
[![coverage report](https://gitlab.com/christian.bobach/codewars-java/badges/master/coverage.svg)](https://gitlab.com/christian.bobach/codewars-java/-/commits/master)

My codewars katas.

[![kata status](https://www.codewars.com/users/cbobach/badges/micro)](https://www.codewars.com/users/cbobach)
